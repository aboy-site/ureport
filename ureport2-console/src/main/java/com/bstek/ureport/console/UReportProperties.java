package com.bstek.ureport.console;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zhaomengru
 * @since 2024/3/19 - 11:00
 */
@ConfigurationProperties(prefix = "ureport")
public class UReportProperties {


    private String baseUrl = UReportSpringUtil.getProperty("spring.application.name");

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
