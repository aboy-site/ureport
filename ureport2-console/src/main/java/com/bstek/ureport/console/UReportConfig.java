package com.bstek.ureport.console;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaomengru
 * @since 2024/3/19 - 11:32
 */
@Configuration
@EnableConfigurationProperties({UReportProperties.class})
public class UReportConfig {
}
