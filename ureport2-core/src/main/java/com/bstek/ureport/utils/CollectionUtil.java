package com.bstek.ureport.utils;

import org.apache.commons.collections.ListUtils;
import org.apache.poi.ss.formula.functions.T;

import java.util.Collection;
import java.util.List;

/**
 * 集合工具类
 *
 * @author zhaomengru
 * @time 2023/8/15 - 21:50
 */
public class CollectionUtil {

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    public static List<T> emptyIfNull(List<T> list) {
        return list == null ? List.of() : list;
    }
}
